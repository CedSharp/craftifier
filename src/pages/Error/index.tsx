import { Link } from 'solid-app-router';
import { Component } from 'solid-js';

const Error: Component = () => (
  <>
    <h2>Oups!</h2>
    <p>No clue where you tried to go...</p>
    <Link href="/">Go back to safety</Link>
  </>
);

export default Error;
