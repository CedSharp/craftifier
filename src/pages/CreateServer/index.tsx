import {
  Accessor,
  Component,
  createSignal,
  For,
  Match,
  Show,
  Switch,
} from 'solid-js';
import { style } from 'solid-js/web';
import { Button } from '../../components/Button';
import { SignalForm } from '../../composable/form';
import {
  useValidation,
  required,
  minLength,
  notAlreadyUsed,
  ValidationSchema,
} from '../../composable/validation';
import styles from './create-server.module.scss';

const SERVER_TYPES = {
  vanilla: 'Vanilla with no mods',
  curseforge: 'Download modpack from CurseForge',
  custom: 'Create custom modpack',
};
type ServerType = keyof typeof SERVER_TYPES;

interface ChangeEvent extends Event {
  currentTarget: HTMLInputElement | HTMLSelectElement;
}

const VALIDATION_SCHEMAS: ValidationSchema[] = [
  { 'Server Type': [required] },
  {},
  {
    'Server Name': [required, minLength(5)],
    'Server Description': [required, minLength(5)],
  },
];

const CreateServer: Component = (props) => {
  const totalSteps = 3;
  const steps = ['Server Type', 'Selection', 'General Settings'];
  const [step, setStep] = createSignal(0);

  const form = new SignalForm();
  form.add('Server Type', createSignal<ServerType>());
  form.add('Server Name', createSignal(''));
  form.add('Server Description', createSignal(''));
  form.add('Max Players', createSignal(20));
  form.add('Allow Flight', createSignal(true));
  form.add('Require valid account', createSignal(true));

  const validations = VALIDATION_SCHEMAS.map((schema) =>
    useValidation(schema).bind(null, form)
  );

  const validateIfCanGoForward = (): boolean => {
    form.clear();

    const errors = validations[step()]();
    if (errors !== null) {
      Object.keys(errors).forEach((field) => {
        form.setError(field, errors[field]);
      });
      return false;
    }

    return true;
  };

  const nextStep = () => {
    if (validateIfCanGoForward()) setStep(Math.min(totalSteps, step() + 1));
  };

  const gotoStep = (s: number) => {
    if (s === step()) return;
    else if (s < step()) setStep(s);
    else if (s > step() + 1) return;
    else {
      if (validateIfCanGoForward()) {
        setStep(s);
      }
    }
  };

  const prevStep = () => setStep(Math.max(0, step() - 1));

  const assign = (name: string) => (ev: ChangeEvent) => {
    if (
      ev.currentTarget.tagName === 'INPUT' &&
      ev.currentTarget.type.toLowerCase() === 'checkbox'
    ) {
      form.set(name, (ev.currentTarget as HTMLInputElement).checked);
    } else {
      form.set(name, ev.currentTarget.value);
    }
  };

  return (
    <>
      <h2 className={styles.title}>Create Server</h2>

      <div className={styles.steps}>
        <For each={steps}>
          {(s, i) => (
            <div
              className={
                i() === step()
                  ? styles.active
                  : i() > step() + 1
                  ? styles.deactivated
                  : ''
              }
              onClick={() => gotoStep(i())}
            >
              {s}
            </div>
          )}
        </For>
      </div>

      <Show when={step() === 0}>
        <h3 className={styles.subtitle}>Server Type</h3>

        {form.renderSelect(
          'Server Type',
          Object.entries(SERVER_TYPES).map(([k, v]) => ({ [v]: k })),
          { name: 'What type of server do you want to create ?' },
          assign('Server Type'),
          'Select type of server...'
        )}
      </Show>

      <Show when={step() === 1}>
        <Switch>
          <Match when={form.get('Server Type') === 'vanilla'}>
            <h3 className={styles.subtitle}>Nothing to do</h3>
            <p>With vanilla, there are no mods to install.</p>
          </Match>
          <Match when={form.get('Server Type') === 'curseforge'}>
            <h3 className={styles.subtitle}>Modpack Selection</h3>
            <p className={styles.description}>
              Select a modpack to install. You will be able to manage the mods
              later after installing the modpack.
            </p>
          </Match>
          <Match when={form.get('Server Type') === 'custom'}>
            <h3 className={styles.subtitle}>Building a custom ModPack</h3>
            <p className={styles.description}>
              Build the modpack of your dreams! You will be able to manage the
              mods later after creating the modpack.
            </p>
          </Match>
        </Switch>
      </Show>

      <Show when={step() === 2}>
        <h3 className={styles.subtitle}>General Server Information</h3>

        {form.renderInput(
          'Server Name',
          'text',
          {
            name: 'Server Name',
            hint: 'Server name should be unique',
          },
          assign('Server Name'),
          'Assign a name to the server'
        )}

        {form.renderInput(
          'Server Description',
          'text',
          {
            name: 'Server Description',
            hint: 'The description is shown on the multiplayer listing menu',
          },
          assign('Server Description'),
          'Describe what the server is about'
        )}

        {form.renderInput(
          'Max Players',
          'number',
          { name: 'The maximum number of players that can connect' },
          assign('Max Players')
        )}

        {form.renderSwitch(
          'Allow Flight',
          {
            name: 'Allow Flight?',
            hint: 'When deactivated, players flying will be kicked',
          },
          assign('Allow Flight')
        )}

        {form.renderSwitch(
          'Require valid account',
          {
            name: 'Require valid account?',
            hint: 'Require players to have a valid minecraft account',
          },
          assign('Require valid account')
        )}
      </Show>

      <div className={styles.actions}>
        <Show when={step() > 0}>
          <Button label="Previous" secondary onClick={prevStep} />
        </Show>

        <div className={styles.right}>
          <Show when={step() < totalSteps - 1}>
            <Button label="Next" primary onClick={nextStep} />
          </Show>
          <Show when={step() === totalSteps - 1}>
            <Button label="Create Server" primary />
          </Show>
        </div>
      </div>
    </>
  );
};

export default CreateServer;
