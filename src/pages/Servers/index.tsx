import type { Component } from 'solid-js';
import './servers.scss';

import ServerList from '../../components/ServerList';

const ServersPage: Component = () => (
  <>
    <h2>Servers</h2>
    <ServerList />
  </>
);

export default ServersPage;
