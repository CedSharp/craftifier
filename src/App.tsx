import type { Component } from 'solid-js';

import { render } from 'solid-js/web';
import { lazy } from 'solid-js';
import { RouteDefinition, useRoutes } from 'solid-app-router';

import Sidebar from './components/Sidebar';
import routes from './routes';

import './App.scss';

const App: Component = () => {
  const Routes = useRoutes(routes as RouteDefinition[]);
  return (
    <>
      <Sidebar />

      <main className="app-view">
        <Routes />
      </main>

      <footer className="app-footer">
        <p>
          Made with <span className="icon">favorite</span> and lots of{' '}
          <span className="icon">coffee</span> by C&eacute;drik Dubois
        </p>
      </footer>
    </>
  );
};

export default App;
