import { SignalForm } from './form';

export type ValidationRule = {
  predicate: (v: unknown) => boolean;
  message: string;
};
export type Validator = (
  data: Record<string, unknown>
) => Record<string, string> | null;

export interface ValidationSchema {
  [fieldName: string]: ValidationRule[];
}

const validate = (
  schema: ValidationSchema,
  data: SignalForm
): Record<string, string> | null => {
  const errors: Record<string, string> = {};
  let hasErrors = false;

  Object.entries(schema).forEach(([field, rules]) => {
    // Validate field exists
    if (!data.has(field)) {
      hasErrors = true;
      errors[field] = `Field ${field} was not provided!`;
      return;
    }

    // Validate each individual rules
    const value = data.get(field);
    for (const { predicate, message } of rules) {
      if (!predicate(value)) {
        hasErrors = true;
        errors[field] = message.replaceAll('%s', field);
        break;
      }
    }
  });

  return hasErrors ? errors : null;
};

export const useValidation = (schema: ValidationSchema): Validator => {
  return validate.bind(null, schema);
};

export const required: ValidationRule = {
  predicate: (v) => {
    switch (Object.prototype.toString.apply(v)) {
      case '[object String]':
        return (v as string).length > 0;
      case '[object Object]':
        return Object.keys(v).length > 0;
      case '[object Array]':
        return (v as unknown[]).length > 0;
      case '[object Undefined]':
      case '[object Null]':
        return false;
      default:
        return true;
    }
  },
  message: 'Field "%s" is required',
};

export const minLength = (length: number): ValidationRule => ({
  predicate: (v) => {
    if (Object.prototype.toString.apply(v) !== '[object String]') return false;
    return (v as string).length >= length;
  },
  message: 'Field "%s" should have at least ' + length + ' characters',
});

export const notAlreadyUsed = (blacklist: unknown[]): ValidationRule => ({
  predicate: (v) => !blacklist.includes(v),
  message: 'Field "%s" cannot already be used',
});
