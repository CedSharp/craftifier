import { createSignal, JSXElement, For, Show, Signal } from 'solid-js';
import Field, { FieldProps } from '../components/Field';

interface SignalFormField {
  signal: Signal<unknown>;
  error: Signal<string | null>;
}

export class SignalForm {
  private fields = new Map<string, SignalFormField>();

  add(name: string, signal: Signal<unknown>): void {
    this.fields.set(name, { signal, error: createSignal(null) });
  }

  get(name: string): unknown {
    if (this.fields.has(name)) return this.fields.get(name).signal[0]();
    throw new Error(`There are no field named "${name}"`);
  }

  set(name: string, value: unknown): void {
    if (this.fields.has(name)) this.fields.get(name).signal[1](value);
    else throw new Error(`There are no field named "${name}"`);
  }

  has(name: string): boolean {
    return this.fields.has(name);
  }

  getError(name: string): string | null {
    if (this.fields.has(name)) return this.fields.get(name).error[0]();
    throw new Error(`There are no field named "${name}"`);
  }

  setError(name: string, error: string): void {
    if (this.fields.has(name)) this.fields.get(name).error[1](error);
    else throw new Error(`There are no field named "${name}"`);
  }

  clear(): void {
    for (const entry of this.fields.values()) entry.error[1](null);
  }

  renderInput(
    name: string,
    inputType: string,
    fieldProps: FieldProps,
    onChange: (ev: Event) => void,
    placeholder = ''
  ): JSXElement {
    if (!this.fields.has(name))
      throw new Error(`There are no field named "${name}"`);

    const field = this.fields.get(name);
    if (fieldProps.error) delete fieldProps.error;

    return (
      <Field {...fieldProps} error={field.error[0]()}>
        <input
          type={inputType}
          onChange={onChange}
          value={field.signal[0]() as string}
          placeholder={placeholder}
        />
      </Field>
    );
  }

  renderSwitch(
    name: string,
    fieldProps: FieldProps,
    onChange: (ev: Event) => void
  ): JSXElement {
    if (!this.fields.has(name))
      throw new Error(`There are no field named ${name}`);

    const field = this.fields.get(name);
    if (fieldProps.error) delete fieldProps.error;

    return (
      <Field
        {...fieldProps}
        error={field.error[0]()}
        switch={{ value: field.signal[0]() as boolean, onChange }}
      />
    );
  }

  renderSelect(
    name: string,
    options: Array<Record<string, string> | string>,
    fieldProps: FieldProps,
    onChange: (ev: Event) => void,
    placeholder = ''
  ): JSXElement {
    if (!this.fields.has(name))
      throw new Error(`There are no field named ${name}`);

    const field = this.fields.get(name);
    if (fieldProps.error) delete fieldProps.error;

    return (
      <Field {...fieldProps} error={field.error[0]()}>
        <select onChange={onChange}>
          <Show when={placeholder}>
            <option selected={!field.signal[0]()} disabled>
              {placeholder}
            </option>
          </Show>
          <For each={options}>
            {(option) => {
              const label =
                typeof option === 'object' ? Object.keys(option)[0] : option;
              const value =
                typeof option === 'object' ? Object.values(option)[0] : option;
              return (
                <option value={value} selected={field.signal[0]() === value}>
                  {label}
                </option>
              );
            }}
          </For>
        </select>
      </Field>
    );
  }
}
