import { lazy } from 'solid-js';

const routes = [
  {
    name: 'Dashboard',
    path: '/',
    icon: 'dashboard',
    component: lazy(() => import('./Counter')),
  },
  {
    name: 'Servers',
    path: '/servers',
    icon: 'dns',
    component: lazy(() => import('./pages/Servers')),
  },
  {
    name: 'Create Server',
    path: '/servers/create',
    icon: 'queue',
    component: lazy(() => import('./pages/CreateServer')),
  },
  { path: '**', component: () => lazy(() => import('./pages/Error')) },
];

export default routes;
