import { Component, createResource, For, Suspense } from 'solid-js';
import { Button } from '../Button';
import './server_list.scss';

enum ServerType {
  Vanilla,
  Forge,
}

interface Server {
  id: string;
  name: string;
  type: ServerType;
  versions: Record<string, string>;
}

const getTypeName = (type: ServerType): string => {
  switch (type) {
    case ServerType.Vanilla:
      return 'Vanilla';
    case ServerType.Forge:
      return 'Forge';
    default:
      return 'Unknown server type';
  }
};

const fetchServers = (): Promise<Server[]> =>
  new Promise((resolve) =>
    setTimeout(
      () =>
        resolve([
          {
            id: '1',
            name: 'Create above and beyond',
            type: ServerType.Forge,
            versions: { minecraft: '1.16.5', forge: 'xxxx' },
          },
          {
            id: '2',
            name: 'Vanilla 1.18.1',
            type: ServerType.Vanilla,
            versions: { minecraft: '1.18.1' },
          },
          {
            id: '3',
            name: 'Gaming Server',
            type: ServerType.Forge,
            versions: { minecraft: '1.18.1', forge: 'xxxx' },
          },
        ]),
      1000
    )
  );

const ServerList = () => {
  const [servers] = createResource(fetchServers);
  return (
    <ul class="server-list">
      <For each={servers()}>
        {(server) => (
          <li>
            <header>
              <h3>{server.name}</h3>
            </header>
            <div>
              <Button icon="play_arrow" />
              <Button icon="stop" />
              <Button icon="restart_alt" />
              <Button icon="tune" />
              <Button icon="delete" />
            </div>
            <footer>
              {getTypeName(server.type)} {server.versions.minecraft}
            </footer>
          </li>
        )}
      </For>
    </ul>
  );
};

const SuspendedServerList: Component = () => (
  <Suspense fallback={<p>Loading...</p>}>
    <ServerList />
  </Suspense>
);

export default SuspendedServerList;
