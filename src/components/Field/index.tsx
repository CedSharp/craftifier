import { Component, Match, Show, Switch } from 'solid-js';

export interface FieldProps {
  name: string;
  error?: string;
  hint?: string;
  required?: boolean;
  switch?: { value: boolean; onChange: (ev: Event) => void };
}

const Field: Component<FieldProps> = (props) => {
  const fieldCls = () => 'field' + (props.error ? ' has-error' : '');
  const nameCls = () =>
    'field-name' + (props.required ? ' field-required' : '');

  return (
    <div className={fieldCls()}>
      <label>
        <Switch>
          <Match when={props.switch}>
            <div className="field-switch">
              <span className={nameCls()}>
                {props.name}
                {props.required ? '*' : ''}
              </span>
              <div>
                <input
                  type="checkbox"
                  checked={!!props.switch.value}
                  onChange={props.switch.onChange}
                />
                <span />
              </div>
            </div>
          </Match>
          <Match when={!props.switch}>
            <span className={nameCls()}>
              {props.name}
              {props.required ? '*' : ''}
            </span>
            {props.children}
          </Match>
        </Switch>
      </label>
      <Show when={props.hint}>
        <span className="field-hint">{props.hint}</span>
      </Show>
      <Show when={props.error}>
        <span className="field-error">{props.error}</span>
      </Show>
    </div>
  );
};

export default Field;
