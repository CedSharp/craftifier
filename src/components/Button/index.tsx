import type { Component } from 'solid-js';
import { mergeProps, Show } from 'solid-js';
import styles from './button.module.scss';

interface ButtonProps {
  loading?: boolean;
  disabled?: boolean;
  icon?: string;
  label?: string;
  type?: 'submit' | 'reset' | 'button';
  onClick?: EventListener;
  className?: string;
  primary?: boolean;
  secondary?: boolean;
}

const DEFAULT_PROPS = {
  type: 'button',
};

export const Button: Component<ButtonProps> = (providedProps) => {
  const props = mergeProps(DEFAULT_PROPS, providedProps);
  const cls = () => {
    const variables = Object.entries({
      [styles.primary]: props.primary,
      [styles.secondary]: props.secondary,
    })
      .filter(([_, v]) => !!v)
      .map(([k]) => k)
      .join(' ');

    let classes = props.className || '';
    classes += (classes ? ' ' : '') + variables;
    return classes;
  };

  return (
    <button type={props.type} className={cls()} onClick={props.onClick}>
      <Show when={props.icon}>
        <span className="icon">{props.icon}</span>
      </Show>
      <Show when={props.label}>
        <span className="label">{props.label}</span>
      </Show>
    </button>
  );
};
