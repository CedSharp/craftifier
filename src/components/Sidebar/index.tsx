import { Component, For } from 'solid-js';
import { Link, useMatch } from 'solid-app-router';
import routes from '../../routes';
import './sidebar.scss';

const Sidebar: Component = () => (
  <aside class="app-sidebar">
    <div className="brand">
      <h1>
        <span className="icon">settings_applications</span>Craftifier
      </h1>
    </div>

    <ul role="navigation">
      <For each={routes.filter((route) => route.name)}>
        {(item) => {
          const currentRoute = useMatch(() => item.path);
          return (
            <li>
              <Link
                href={item.path}
                classList={{ active: Boolean(currentRoute()) }}
              >
                {item.icon && <span className="icon">{item.icon}</span>}
                {item.name}
              </Link>
            </li>
          );
        }}
      </For>
    </ul>
  </aside>
);

export default Sidebar;
