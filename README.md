# Craftifier

A simple minecraft server management dashboard with some curseforge magic!

## Roadmap

Here are the features I plan to implement in the admin:

- [ ] view a list of all configured servers
- [ ] Start, stop and restart a server
- [ ] Access a running's server console
- [ ] View stats related to a server (cpu, ram, network)
- [ ] Create a new vanilla server
- [ ] Download and start a curseforge server modpack
- [ ] Create a custom modpack, and instantiate a server from it

## Developement

Project was built with NodeJS v17.8.0 and SolidJS.

The dependencies can be installed with any package manager (npm, yarn, pnpm):

```shell
$ npm i
$ yarn
$ pnpm i
```

The available scripts are the following:

| Script | Description                                          |
| :----- | :--------------------------------------------------- |
| dev    | Starts a ViteJS development server (lightning fast!) |
| build  | Generates the production build in `./dist`           |
| serve  | Runs the previously generated build in `./dist`      |
